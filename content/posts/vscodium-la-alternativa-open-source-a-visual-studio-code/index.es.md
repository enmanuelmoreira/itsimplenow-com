---
title: "VSCodium, la alternativa Open Source a Visual Studio Code"
date: 2021-07-21
lastmod: 2021-07-29
author: Enmanuel Moreira
description: "A pesar de que el código fuente de Visual Studio Code está bajo licencia MIT, el software ya compilado y disponible para su descarga tiene una licencia diferente lo que la hace incompatible con el modelo FLOSS y además de eso contiene captura de datos por telemetría. VSCodium nace como la opción de Software Libre que empaqueta el código de VSCode y lo distribuye sin la telemetría."
draft: false

resources:
- name: "featured-image"
  src: "featured-image.jpeg"

categories: ["Linux", "Open Source"]
tags: ["vscode","vscodium","tutoriales"]

lightgallery: true
---

<!--more-->

## PROMO DigitalOcean

***

Antes de comenzar, quería contarles que hay una promoción en DigitalOcean donde te dan un crédito de USD 100.00 durante 60 días para que puedas probar los servicios que este Proveedor Cloud ofrece. Lo único que tienes que hacer es suscribirte a DigitalOcean con el siguiente enlace: <https://bit.ly/digitalocean-itsm>

***

A pesar de que el código fuente de Visual Studio Code está bajo licencia MIT, el software ya compilado y disponible para su descarga tiene una licencia diferente lo que la hace incompatible con el modelo FLOSS y además de eso contiene captura de datos por telemetría. Según podemos leer en un **[comentario](https://github.com/Microsoft/vscode/issues/60#issuecomment-161792005)** de un mantenedor de VSCode:  

> When we [Microsoft] build Visual Studio Code, we do exactly this. We clone the vscode repository, we lay down a customized product.json that has Microsoft specific functionality (telemetry, gallery, logo, etc.), and then produce a build that we release under our license.
When you clone and build from the vscode repo, none of these endpoints are configured in the default product.json. Therefore, you generate a “clean” build, without the Microsoft customizations, which is by default licensed under the MIT license

Los binarios oficiales de VSCode no son las mismas que del código fuente ya que ya que incluyen componentes para rastrear las acciones del editor y enviar telemetría.  

VSCodium nace como la opción de Software Libre que empaqueta el código de VSCode directamente desde el repositorio en GitHub (que contiene la licencia MIT) y lo distribuye sin la telemetría. En esencia, será básicamente el mismo Visual Studio Code y por lo tanto, funciona de la misma manera, con todas las características y el soporte presente en el proyecto principal, con otra ligera diferencia: el icono.  

Hoy te voy a enseñar como instalar VSCodium en Linux.  

## Instalando VSCodium  

***

### Debian / Ubuntu

Añadimos la clave GPG del repositorio:  

```bash
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
```

Añadimos el repositorio:  

```bash
echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list
```

Actualizamos el listado de paquetes e instalamos VSCodium:  

```bash
sudo apt update && sudo apt install codium
```

### Fedora / RHEL / OpenSUSE

Añadimos la llave GPG del repositorio:  

```bash
sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
```

- Fedora/ RHEL / CentOS / AlmaLinux / Rocky Linux

Añadimos el repositorio:  

```bash
printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=gitlab.com_paulcarroty_vscodium_repo\nbaseurl=https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg" |sudo tee -a /etc/yum.repos.d/vscodium.repo
```

Instalamos VSCodium:

```bash
sudo dnf install codium
```

- OpenSUSE / SUSE

Añadimos el repositorio:  

```bash
printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=gitlab.com_paulcarroty_vscodium_repo\nbaseurl=https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg" |sudo tee -a /etc/zypp/repos.d/vscodium.repo
```

Instalamos VSCodium

```bash
sudo zypper in codium
```

### Arch Linux / Manjaro

VSCodium está disponible en AUR (Arch User Repository), y puede ser instalado con con yay:  

```bash
yay -S vscodium-bin
```

## Habilitando VS Marketplace

***

Por defecto, el repositorio de extensiones de Microsoft se encuentra deshabilitado. Si usas alguna extensión y no aparece para instalar en VSCodium, podemos habilitarlo si seguimos los siguientes pasos:  

### Opción 1  

Creamos el archivo product.json en el directorio /.config/VSCodium  

```bash
vim ~/.config/VSCodium/product.json
```

Y pegamos lo siguiente:  

```json
{
  "extensionsGallery": {
    "serviceUrl": "https://marketplace.visualstudio.com/_apis/public/gallery",
    "cacheUrl": "https://vscode.blob.core.windows.net/gallery/index",
    "itemUrl": "https://marketplace.visualstudio.com/items",
    "controlUrl": "",
    "recommendationsUrl": ""
  }
}
```

Guardamos los cambios.  

### Opción 2  

A través de variables de entorno, pegamos lo siguiente en nuestro archivo ~/.bashrc o ~./zshrc:  

```bash
VSCODE_GALLERY_SERVICE_URL='https://marketplace.visualstudio.com/_apis/public/gallery'
VSCODE_GALLERY_CACHE_URL='https://vscode.blob.core.windows.net/gallery/index'
VSCODE_GALLERY_ITEM_URL='https://marketplace.visualstudio.com/items'
VSCODE_GALLERY_CONTROL_URL=''
VSCODE_GALLERY_RECOMMENDATIONS_URL=''
```

Guaramos los cambios y recargamos las variables de entorno:  

- bash
  
```bash
source ~/.bashrc
```

- zsh

```bash
source ~/.zshrc
```

En cualquiera de las dos opciones, tenemos que reiniciar VSCodium y ya tendremos el Marketplace funcionando.  

![VSCodium con Extensiones](/images/vscodium/vscodium.jpg)

Si aún así prefieres Visual Studio Code, puedes deshabilitar la telemetría en los binarios que distribuye Microsoft añadiendo en el JSON del usuario (Archivo -> Preferencias -> Configuraciones) la línea:

```json
"telemetry.enableTelemetry": false
```

Espero les haya gustado este tutorial, ¡hasta la próxima!

## Apoya este Proyecto!!!

Si te pareció útil este artículo y el proyecto en general, considera brindarme un café :)

<style>.bmc-button img{height: 34px !important;width: 35px !important;margin-bottom: 1px !important;box-shadow: none !important;border: none !important;vertical-align: middle !important;}.bmc-button{padding: 7px 15px 7px 10px !important;line-height: 35px !important;height:51px !important;text-decoration: none !important;display:inline-flex !important;color:#ffffff !important;background-color:#5F7FFF !important;border-radius: 8px !important;border: 1px solid transparent !important;font-size: 24px !important;letter-spacing: 0.6px !important;box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;margin: 0 auto !important;font-family:'Cookie', cursive !important;-webkit-box-sizing: border-box !important;box-sizing: border-box !important;}.bmc-button:hover, .bmc-button:active, .bmc-button:focus {-webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;text-decoration: none !important;box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;opacity: 0.85 !important;color:#ffffff !important;}</style><link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet"><a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/enmanuelmoreira"><img src="https://cdn.buymeacoffee.com/buttons/bmc-new-btn-logo.svg" alt="Buy me a coffee"><span style="margin-left:5px;font-size:24px !important;">Buy me a coffee</span></a>
